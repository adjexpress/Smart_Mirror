import QtQuick 2.9
import QtQuick.Controls 2.2
//import QtGraphicalEffects 1.0



    Rectangle{

        property color layer1Color: "#502254"
        property color buttonColor: "white"
        property color onHoverColer: "gold"
        property color borderColor: "lightgreen"

        signal record()



        id:layer1
        width: Math.min(parent.width,parent.height)/6
        height: width
        border.width: 1
        border.color: borderColor
//            Gradient{
//            GradientStop{position: 0.0 ;color: "blue"}
//            GradientStop{position: 0.5 ;color: "yellow"}
//        }

        radius: width/2
        color: layer1Color
        Behavior on color { ColorAnimation { duration: 55  }}

        Rectangle{



            id:layer2
            width: parent.width/4*3
            height: width
            radius: width/2
            anchors.centerIn: parent

            Image {

                id: mic
                antialiasing: true
                source: "qrc:///images/mic.png"
                width: parent.width/3*2
                height: width
                fillMode: Image.PreserveAspectFit
                anchors.centerIn: parent
            }

            MouseArea {
                id: mousePad
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    layer1.border.color = onHoverColer
                    layer1.color =  Qt.lighter(layer1Color , 1.3)}

                onExited: {
                    layer1.border.color = borderColor
                    layer1.color = layer1Color}
                onClicked: record()

            }
            color: mousePad.pressed ? Qt.darker(buttonColor , 1.3) : buttonColor
            Behavior on color { ColorAnimation { duration: 55  }     }

            scale: mousePad.pressed ? 1.1 : 1
            Behavior on scale { NumberAnimation { duration: 55 }    }
        }

   }
