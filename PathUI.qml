import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Shapes 1.12
import "./MediaPlayer"
Rectangle{
    id:pathRoot
    //color: "transparent"
    anchors.fill: parent
    property alias pt: pth

    function action(name){
        switch (name){
        case "video":
            mp.loadVideo()
            break
        case "music":
            mp.loadAudio()
            break
        default: break;
        }
    }

    ListModel {
        id:model
        ListElement{
            name: "video"
            icon: "qrc:/images/video.svg"
            //source:
        }
        ListElement{
            name: "music"
            icon: "qrc:/images/music.svg"
            //source:
        }
        ListElement{
            name: "home"
            icon: "qrc:/images/smart-home.svg"
            //source:
        }
    }

    Component{
        id: delegate
        Column{
            id:asd
            property bool crnt: PathView.isCurrentItem
            opacity: crnt ? 1 : 0.5
            scale: crnt ? 1 : 0.5
            /*switch(Math.abs(PathView.currentItem.index-index-model.count)){
                   case 0 :
                       console.log("0");
                       break
                   case 1 :
                       console.log("1");
                       break
                   case 2 :
                       console.log("2");
                       break
                   default:
                       console.log("result: "+Math.abs(PathView.currentIndex-index-model.count))
                       console.log("active item index: "+PathView.currentIndex)
                       console.log("current item index: "+index)
                       console.log("model count: "+model.count)
                       break;
                   }*/
            Behavior on scale { NumberAnimation { duration: 200 }    }
            Behavior on opacity { NumberAnimation { duration: 200 }    }

            Image {
                id: img
                source: icon
                fillMode: Image.PreserveAspectFit
                Behavior on scale { NumberAnimation { duration: 100 }    }
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: img.scale = 1.1
                    onExited: img.scale = 1
                    onClicked: crnt ? action(name) : pth.currentIndex=index
                }
            }
            /*Text {
                id: txt
                text: name
                anchors.horizontalCenter: img.horizontalCenter
                font.pixelSize: Math.round(Screen.pixelDensity)*4
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: txt.color = Qt.lighter(txt.color)
                    onExited: txt.color = Qt.darker(txt.color)
                    onClicked: PathView.currentItem= delegate
                }
            }*/
        }
    }


//    Shape{
//        anchors.fill: parent
//        ShapePath {
//            strokeWidth: 4
//            strokeColor: "cyan"
//            fillGradient: LinearGradient {
//                x1: 20; y1: 20
//                x2: 180; y2: 130
//                GradientStop { position: 0; color: "blue" }
//                GradientStop { position: 0.2; color: "green" }
//                GradientStop { position: 0.4; color: "red" }
//                GradientStop { position: 0.6; color: "yellow" }
//                GradientStop { position: 1; color: "cyan" }
//            }
//            strokeStyle: ShapePath.SolidLine
//            //dashPattern: [ 1, 4 ]

////            startX:parent.width/2; startY: parent.height/2
////            PathLine { x:-parent.width; y:parent.height/2 }
////            PathQuad { x:2*parent.width; y: parent.height/2; controlX: parent.width/2; controlY: parent.height/5 }
////            PathLine { x:parent.width/2; y:parent.height/2 }
//            startX: parent.width/2 //120;
//            startY: parent.height*3/5//100
//            PathQuad { x: parent.width/2 /*120*/; y: parent.height*1/5; controlX: -parent.width*1/10; controlY: parent.height*1/5 }
//            PathQuad { x: parent.width/2 /*120*/; y: parent.height*3/5; controlX: parent.width*11/10; controlY: parent.height*1/5 }
//        }
//    }

    PathView {
        id:pth
        anchors.fill: parent
        clip: true
        model: model
        delegate: delegate
        flickDeceleration: 50
        path: Path {

            startX: parent.width/2 //120;
            startY: parent.height*3/5//100
            PathQuad { x: parent.width/2 /*120*/; y: parent.height*1/5; controlX: -parent.width*1/10; controlY: parent.height*1/5 }
            PathQuad { x: parent.width/2 /*120*/; y: parent.height*3/5; controlX: parent.width*11/10; controlY: parent.height*1/5 }
            //            startX: parent.width/2 //120;
            //            startY: parent.height*3/4//100
            //            PathQuad { x: parent.width/2 /*120*/; y: parent.height*1/4; controlX: parent.width*1/10; controlY: parent.height*3/5 }
            //            PathQuad { x: parent.width/2 /*120*/; y: parent.height*3/4; controlX: parent.width*9/10; controlY: parent.height*3/5 }
            //            startX: 120; startY: 100
            //            PathQuad { x: 120; y: 25; controlX: 260; controlY: 75 }
            //            PathQuad { x: 120; y: 100; controlX: -20; controlY: 75 }
        }
    }
}
