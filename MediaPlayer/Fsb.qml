import ".."
import QtQuick 2.12
import QtQuick.Controls 2.5
import Qt.labs.folderlistmodel 2.12
import QtQml.Models 2.12
import QtQuick.Window 2.12

Rectangle {
    id:fileBrowser
    color: "transparent"
//    width: applicationWindowWidth
//    height: applicationWindowHeight
    z:4
    SystemPalette {
        id:palette
    }


    property string source
    property var filters

    signal fileSelected(string fileName)
    signal canceled()



    Loader{
        id:fsbLoader
        onActiveChanged: console.log("loadFm finished")
    }
    function loadFm() {
        fsbLoader.sourceComponent= fileBrowserComponent
        fsbLoader.item.folderName= fileBrowser.source
        fsbLoader.item.typeFilter= fileBrowser.filters
        fsbLoader.item.parent= fileBrowser
        fsbLoader.item.focus = true
        fsbLoader.item.anchors.fill= fileBrowser
    }

    Component{
        id:fileBrowserComponent

        Rectangle {
            id:root
            property int itemsHeight: Math.min(height,width)
            property color textColor: palette.text//"gold"
            property color bgcolor: palette.base //"black"
            property color itemColor: "transparent"
            property bool showFocusHighlight: false

            property string folderName
            property var typeFilter
            property var activFolder : fsModel.parentFolder.toString().split("/");


            color: bgcolor


            FolderListModel {
                id:fsModel
                folder: folderName
                nameFilters: filters
                showDirsFirst: true
                onFolderChanged: activFolder = fsModel.folder.toString().split("/")
            }


            Component {
                id:folderDelegate

                Rectangle {
                    id: listItem
                    width: listItem.ListView.view.width//root.width
                    height: txt.contentHeight*4/3
                    color: itemColor
                    /////////////////////////////////////////////////////////////////
                    //// act() function will be write here in folder item delegate
                    //// separat from functions Area
                    //// to work with both Windows &  unix like OS
                    /////////////////////////////////////////////////////////////////

                    function act(){
                        var path="file://";
                        if (filePath.length > 2 && filePath[1] === ':')
                            path += '/';
                        path += filePath
                        if (fsModel.isFolder(index))
                            down(path)
                        else
                            select(path)
                    }


                    Rectangle {
                        id: highlight; visible: false
                        anchors.fill: parent
                        color: palette.highlight
                        opacity: 0.5
                        gradient: Gradient {
                            GradientStop { id: t0; position: 0.0; color: Qt.lighter(palette.highlight) }
                            GradientStop { id: t1; position: 0.5; color: palette.highlight }
                            GradientStop { id: t2; position: 1.0; color: Qt.lighter(palette.highlight) }
                        }
                    }

                    Rectangle {
                        id:icon
                        width: height
                        height: parent.height
                        anchors.left: parent.left
                        color: "transparent"
                        Image {
                            source: "qrc:///images/folder.svg"
                            fillMode: Image.PreserveAspectFit
                            anchors {
                                margins: 2
                                centerIn: parent
                            }
                            visible: fsModel.isFolder(index)
                        }
                    }

                    Text {
                        id:txt
                        text: fileName
                        anchors {
                            fill: parent
                            leftMargin: icon.width*1.5
                            horizontalCenter: parent.horizontalCenter
                        }
                        color: textColor
                    }

                    MouseArea {
                        id:mousPad
                        anchors.fill: parent
                        hoverEnabled: true
                        onEntered:
                            highlight.visible = true
                        onExited:
                            highlight.visible = false
                        onClicked: {
                            //listItem.ListView.view.currentIndex= index;
                            act();
                        }
                    }

                    states: [
                        State {
                            name: "pressed"
                            when: mousPad.pressed
                            PropertyChanges { target: highlight; visible: true }
                            PropertyChanges { target: txt; color: palette.highlightedText }
                        }
                    ]
                }
            }


            Keys.onPressed: {
                root.keyHandler(event.key)
                if(event.key=== Qt.Key_Enter || event.key === Qt.Key_Return || event.key === Qt.Key_Space){
                    list.currentItem.act()
                    event.accepted = true
                }
                else if (event.key === Qt.Key_Escape || event.key === Qt.Key_Back)
                    up();
            }

            Rectangle {
                id:titleBar
                width: parent.width
                height: parent.height/11
                color: "gray"
                opacity: 0.9
                z:2
                Text {
                    id: activePath
                    text: activFolder[activFolder.length - 1]
                    font.pixelSize: Screen.PixelDensity*5
                    color: "gold"
                    elide: Text.ElideMiddle
                    anchors.centerIn: parent
                    //text: list.activeFocus ? "I have active focus!" : "I do not have active focus"
                }

                Rectangle {
                    id:backBtn
                    height: parent.height
                    width: height*3/4
                    opacity: 0.9
                    Image {
                        id: bkIcon
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/images/back.svg"
                    }
                    MouseArea {
                        id:bkClick
                        height: parent.height*5
                        width: height
                        anchors.centerIn: parent
                        //anchors.fill: parent
                        onClicked: up()
                    }
                }

                Rectangle {
                    id:closeBtn
                    height: parent.height
                    width: height
                    anchors.right: parent.right
                    opacity: 0.9
                    Image {
                        id: clsIcon
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/images/close.svg"
                    }
                    MouseArea {
                        id:xClick
                        height: parent.height*5
                        width: height
                        anchors.centerIn: parent
                        //anchors.fill: parent
                        onClicked: cancel()
                    }
                }
            }

            ListView { //must be completed
                id:list
                anchors { top: titleBar.bottom ; bottom: parent.bottom; right:parent.right; left: parent.left; margins:  Screen.pixelDensity}
                model: DelegateModel {
                    id:dlgModel
                    model: fsModel
                    delegate: folderDelegate
                }

                highlight: Rectangle {
                    id: highlight; visible: root.showFocusHighlight
                    anchors.fill: parent
                    color: palette.highlight
                    opacity: 0.9
                    gradient: Gradient {
                        GradientStop { id: t1; position: 0.0; color: palette.highlight }
                        GradientStop { id: t2; position: 1.0; color: Qt.lighter(palette.highlight) }
                    }
                }
                highlightMoveVelocity: 1000
                pressDelay: 100
                focus: true
                interactive: true
                keyNavigationEnabled: true
                Keys.onPressed: root.keyHandler(event.key)
            }


            /****************************************************************/
            /*  Begien Of Function Area:
            /*
            /*  here will be decleare all function needed for <file browser>
            /****************************************************************/




            function select(path) { //finallize the job
                if(path !== ""){
                    currentPath= fsModel.folder // next time must be change to folderDelegate.listview.view.model
                    fileSelected(path)
                    fsbLoader.sourceComponent = undefined
                }
                // must add a Loader and set Loader.source to undefined
            }

            function cancel(){
                canceled()
                fsbLoader.sourceComponent = undefined
                // must add a Loader and set Loader.source to undefined
                return
            }

            function down(path) {
                fsModel.folder = path
            }
            function up() {
                var path = fsModel.parentFolder;
                if (path.toString().length === 0 || path.toString() === 'file:///')
                    return;
                fsModel.folder=fsModel.parentFolder
            }

            function keyHandler(key){
                switch (key) {
                case Qt.Key_Up:
                case Qt.Key_Down:
                case Qt.Key_Left:
                case Qt.Key_Right:
                    root.showFocusHighlight = true;
                    break;
                default:
                    // do nothing
                    break;
                }
            }


            function createPl(){

            }

            /***********************
            /*  End Of Function Area
            /**********************/

        }
    }
}
