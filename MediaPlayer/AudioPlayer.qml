import ".."
import QtQuick 2.12
import QtMultimedia 5.12

VideoOutput {
    id: videoOutput
    source: player
    anchors.fill: parent
    property string mediaSource
    property string position
    property string title
    //property var coverImg: player.metaData.coverArtUrlLarge

    signal finished()

    function start(){ player.play() }
    function pause(){ player.pause()}
    function stop() { player.stop() }
    function seek(position) {
        if (player.seekable)
            player.seek(position)
    }
    Rectangle{
        width: parent.width/3
        height: width
        anchors.centerIn: parent
        color: "gray"
        Image {
            id: cover
            anchors.fill: parent
            source: player.metaData.coverArtImage
        }
    }

    MediaPlayer {
        id:player
        autoPlay: false
        source: mediaSource
        onStopped: finished()
    }

    MouseArea{
        anchors.fill: parent
        hoverEnabled: true
        onMouseXChanged:  audioCtl.visible = true
        onClicked:
        {
            switch (player.playbackState) {
            case MediaPlayer.PlayingState :
                videoOutput.pause()
                console.log("clicked")
                break
            case MediaPlayer.PausedState :
                videoOutput.start()
                console.log("clicked")
                break
            default:
                console.log("clicked")
                console.log("none of cases")
                break
            }
        }
        //onDoubleClicked: finished()
    }

    Controls{
        id:audioCtl
        anchors.fill: parent
        onClose: finished()
        onPlay: videoOutput.start()
        onPause: videoOutput.pause()
        playStatus: player.playbackState === MediaPlayer.PlayingState
        duration: player.duration
        playPosition: player.position
    }
}
