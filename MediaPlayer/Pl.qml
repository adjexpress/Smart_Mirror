import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick 2.12
import QtMultimedia 5.12
import ".."

Rectangle {
    id: ctrlRoot
    color: "transparent"

    property bool playStatus: true
    property int duration
    property int playPosition
    property int visableTime : 3000
    property string title
    property real aoudioVolume: volumeSlider.volume

    onVisibleChanged: hideControls.start()

    signal close()
    signal play()
    signal pause()
    signal next()
    signal previous()

    Timer {
        id:hideControls
        interval: visableTime
        repeat: false
        onTriggered: ctrlRoot.visible = false
    }

    Rectangle {
        id:titleBar
        width: parent.width
        height: parent.height/11
        anchors.top: parent.top
        color: "transparent"
        opacity: 0.9
        z:2
        Text {
            id: info
            text: title
            anchors.centerIn: parent
        }

        Rectangle {
            id:backBtn
            height: parent.height
            width: height*3/4
            color: "transparent"
            Image {
                id: bkIcon
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/back.svg"
            }
            MouseArea {
                id:bkClick
                height: parent.height*5
                width: height
                anchors.centerIn: parent
                //anchors.fill: parent
                onClicked: loadVideo()//loadAudio()
            }
        }

        Rectangle {
            id:closeBtn
            height: parent.height
            width: height
            anchors.right: parent.right
            color: "transparent"
            Image {
                id: clsIcon
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/close.svg"
            }
            MouseArea {
                id:xClick
                height: parent.height*5
                width: height
                anchors.centerIn: parent
                //anchors.fill: parent
                onClicked: close()
            }
        }
    }

    Rectangle{
        id:mainControls
        implicitWidth: parent.width
        implicitHeight: Screen.pixelDensity*12
        anchors.bottom: parent.bottom
        opacity: 0.9
        //SeekBar
        SeekBar{
            id:seekBar
            anchors{ bottom: parent.top; left: parent.left; right: parent.right}
            duration: ctrlRoot.duration
            playPosition: ctrlRoot.playPosition
            onSeekPositionChanged: ctrlRoot.parent.seek(seekPosition)
        }
        //controls
        Rectangle{
            id:botControls
            anchors.fill: parent

            //playcontrol
            Row{
                id:playControls
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: Screen.pixelDensity*5
                padding: Screen.pixelDensity*2
                spacing: Screen.pixelDensity
                ButtonSmpl{
                    width: Screen.pixelDensity*10
                    height: width
                    radius: Screen.pixelDensity*2
                    buttonColor: "transparent"
                    buttonLabel: ""
                    onButtonClicked: previous()
                    Image {
                        id: img1
                        anchors.fill: parent
                        antialiasing: true
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/images/m_pre.svg"
                        mipmap: true
                    }

                }
                ButtonSmpl{
                    width: Screen.pixelDensity*10
                    height: width
                    radius: Screen.pixelDensity*2
                    buttonColor: "transparent"
                    buttonLabel: ""
                    onButtonClicked: play()
                    Image {
                        id: img2
                        anchors.fill: parent
                        antialiasing: true
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/images/m_play.svg"
                        mipmap: true
                    }
                }
                ButtonSmpl{
                    width: Screen.pixelDensity*10
                    height: width
                    radius: Screen.pixelDensity*2
                    buttonColor: "transparent"
                    buttonLabel: ""
                    onButtonClicked: pause()
                    Image {
                        id: img3
                        anchors.fill: parent
                        antialiasing: true
                        fillMode: Image.Stretch
                        source: "qrc:/images/m_pause.svg"
                        mipmap: true
                    }
                }
                ButtonSmpl{
                    width: Screen.pixelDensity*10
                    height: width
                    radius: Screen.pixelDensity*2
                    buttonColor: "transparent"
                    buttonLabel: ""
                    onButtonClicked: next()
                    Image {
                        id: img4
                        anchors.fill: parent
                        antialiasing: true
                        fillMode: Image.Stretch
                        source: "qrc:/images/m_next.svg"
                        mipmap: true
                    }
                }
            }

            //soundLevel
            Row{
                id: soundControls
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: Screen.pixelDensity*5
                padding: Screen.pixelDensity*2
                spacing: Screen.pixelDensity
                ButtonSmpl{
                    width: Screen.pixelDensity*10
                    height: width
                    radius: Screen.pixelDensity*2
                    buttonColor: "transparent"
                    buttonLabel: ""
                    Image {
                        //id: name
                        source: "file"
                    }
                }
                Slider {
                    id: volumeSlider
                    from: 0.0
                    to: 1.0
                    value: 0.75
                    property real volume : QtMultimedia.convertVolume(volumeSlider.value,QtMultimedia.LogarithmicVolumeScale,QtMultimedia.LinearVolumeScale)
                    anchors.verticalCenter: parent.verticalCenter
                    implicitWidth: Screen.pixelDensity*30
                    implicitHeight: Screen.pixelDensity*5
                    clip: true
                    handle: Rectangle{
                        width: 0
                        height: 0
                    }
                    background: Rectangle{

                        anchors.fill: parent
                        radius: Screen.pixelDensity*2
                        color: "lightGray"
                        Rectangle{
                            //anchors.verticalCenter:  parent.verticalCenter
                            anchors.left: parent.left;
                            anchors.top: parent.top; anchors.bottom: parent.bottom
                            //height: parent.height - Screen.pixelDensity
                            width: volumeSlider.visualPosition *parent.width - Screen.pixelDensity
                            //anchors.margins: Screen.pixelDensity/2
                            //radius: Screen.pixelDensity
                            color: "skyBlue"
                        }
                    }
                }
            }
        }
    }
}











/////
// popup test
/////
//ApplicationWindow {
//    id: window
//    width: 400
//    height: 400
//    visible: true

//    Button {
//        text: "Open"
//        onClicked: popup.open()
//    }

//    Popup {
//        id: popup
//        anchors.centerIn: parent
//        width: parent.width/3
//        height: width

//        modal: true
//        focus: true
//        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
//        contentItem: WebEngineView{
//            id: homePanel
//            anchors.fill: parent
//            backgroundColor: "black"
//            Component.onCompleted: url= "http://www.devsg.ir"
//            onFeaturePermissionRequested: grantFeaturePermission(securityOrigin,WebEngineView.MediaAudioCapture,true)
//        }
//    }
//}







//Item {
//    width: 400;
//    height: 300;

//    FolderListModel {
//                folder: "/home/abdorahmanamani/Qt/WorkSpace/scketch_1/MediaPlayer/"
//                id: folderModel
//                nameFilters: ["*.ogg"]
//                showDirs: false
//                showDotAndDotDot: false
//            }
//    ListModel {
//        id:lst

//    }

//    Audio {
//        id: player;
//        playlist: m
//    }

//    ListView {
//        model: playlist;
//        delegate: Text {
//            font.pixelSize: 16;
//            text: source;
//        }
//    }

//    MouseArea {
//        anchors.fill: parent;
//        onPressed: {
//            if (player.playbackState != Audio.PlayingState) {
//                player.play();
//            } else {
//                player.pause();
//            }
//        }
//    }
//}
