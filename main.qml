import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtWebView 1.0
import "./MediaPlayer"
import "./Settings"


ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 720
    height: width*9/16
    title: qsTr("Smart Mirror")
    color: "black"


    MPlayer{
        id:mediaPlayer
        z:3
        anchors.fill: parent
    }

    Clock{
        anchors.fill: parent
        z:2
    }
    //property type name: value
    property alias applicationWindowHeight: applicationWindow.height
    property alias applicationWindowWidth: applicationWindow.width
    property alias mp: mediaPlayer
    //property alias appS: Setting.a


//    PathUI{
//        id: view
//                anchors.fill: parent
//                pt.snapMode: PathView.SnapToItem
//    }

    SweepUI{
        anchors.fill: parent
    }



//    PathView {
//        id: view
//        anchors.fill: parent
//        snapMode: PathView.SnapToItem
//        currentIndex: indicator.currentIndex
//        model: ["MediaPlayer/MPlayer.qml","WebPage.qml"]
//        delegate: Loader {
//            width: parent.width
//            height: parent.height
//            source: modelData
//        }
//        path: Path {
//            startX:view.width/2; startY: view.height/2
//            PathQuad { x:view.width/2; y: view.height/2; controlX: 3*view.width/2; controlY: view.height/2 }
//            PathQuad { x: view.width/2; y: view.height/2; controlX: -2*view.width; controlY: view.height/2 }
//        }
//    }
//    PageIndicator {
//        id: indicator
//        count: view.count
//        interactive: true
//        currentIndex: view.currentIndex
//        anchors.bottom: parent.bottom
//        anchors.horizontalCenter: parent.horizontalCenter
//    }

        /*SwipeView {
            id: view
            anchors.fill: parent
            currentIndex: indicator.currentIndex
            //FDlg {}
            MPlayer{ focus: true}
            WebPage {id: webpage;}
        }*/

    VoiceCmd{
        id:pp
        enter: Transition {
            NumberAnimation { property: "opacity"; duration: 500 ; easing.type: Easing.Linear; from: 0.0; to: 1.0 }
            NumberAnimation { property: "width"; duration: 700 ; easing.type: Easing.OutBounce; from: 20 ; to: pp.parent.width/3 }
            NumberAnimation { property: "height"; duration: 700 ; easing.type: Easing.OutBounce; from: 20 ; to: pp.parent.width/3 }
            NumberAnimation { property: "x"; duration: 700 ; easing.type: Easing.OutBounce; from: recbtn.centerX ; to: pp.parent.width/3 }
            NumberAnimation { property: "y"; duration: 700 ; easing.type: Easing.OutBounce; from: recbtn.centerY; to: pp.parent.width/9 }
        }
        exit: Transition {
            NumberAnimation { property: "opacity"; duration: 700 ; easing.type: Easing.Linear; from: 1.0; to: .0 }
            NumberAnimation { property: "width"; duration: 500 ; easing.type: Easing.Linear; to: 20 }
            NumberAnimation { property: "height"; duration: 500 ; easing.type: Easing.Linear; to: 20  }
            NumberAnimation { property: "x"; duration: 500 ; easing.type: Easing.InQuint;  to: recbtn.centerX }
            NumberAnimation { property: "y"; duration: 500 ; easing.type: Easing.InQuint;  to: recbtn.centerY }
        }
    }

    RecordButton{
        id:recbtn
        property int centerX: x+width/2
        property int centerY: y+height/2
        anchors{
            bottomMargin: 10
            leftMargin: 10
            bottom: parent.bottom
            left: parent.left
        }
        onRecord: pp.open()
    }
}
