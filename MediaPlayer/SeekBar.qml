import".."
import QtQuick 2.12
import QtQuick.Window 2.12

Rectangle{
    id:seekBarRoot

    property int seekPosition
    property int duration
    property int playPosition
    property bool seeking: false
    property bool controlMode

    signal seek(int seekPosition)

    height: Math.round(Screen.pixelDensity*5)

    Rectangle {
        id:bkGrnd
        width: parent.width
        height: parent.height//parent.height/3
        anchors.centerIn: parent
        color: "gray"
        opacity: 0.7
        radius: Math.round(Screen.pixelDensity)

        MouseArea {
            id:seekArea
            width: parent.width
            height: parent.height*5
            anchors.horizontalCenter: parent.horizontalCenter
            hoverEnabled: true
            onEntered: {seekHandle.scale = 1.5; /*seekHandle.opacity = 0.5*/}
            onExited: {seekHandle.scale = 1; seekHandle.opacity = 1}
            onClicked: {
                seekBarRoot.seekPosition = seekBarRoot.duration * seekArea.mouseX / bkGrnd.width
            }
        }
    }
    

    Rectangle{
        id:seekProgress
        height: bkGrnd.height//parent.height/3
        anchors { left: bkGrnd.left; top: bkGrnd.top; bottom: bkGrnd.bottom;}
        width: bkGrnd.width * playPosition/duration
        color: "skyBlue"
        opacity: 0.7
        radius: Math.round(Screen.pixelDensity)
    }

//    Rectangle {
//        id:seekHandle
//        height: seekProgress.height//parent.height*3/2
//        width: height/3
//        radius: height/5
//        color: "#ececf1"
//        visible: controlMode ? false : true
//        anchors{verticalCenter: seekProgress.verticalCenter}
//        x: seekProgress.width-width/2
//        MouseArea {
//            id: seekHandleArea
//            width: parent.width*2
//            height: width
//            hoverEnabled: true
//            onEntered: {seekHandle.scale = 1.5; /*seekHandle.opacity = 0.5*/}
//            onExited: {seekHandle.scale = 1; seekHandle.opacity = 1}
//            drag {
//                target: seekHandle
//                axis: Drag.XAxis
//                minimumX: 0
//                maximumX: bkGrnd.width
//            }

//            onPressed: {
//                seeking= true
//                seekHandle.scale = 1.5
//                //seekHandle.opacity = 0.7
//            }
//            onCanceled: {
//                seekBarRoot.seekPosition = seekBarRoot.duration * seekHandle.x / bkGrnd.width
//                seeking =false
//            }

//            onReleased: {
//                seekBarRoot.seekPosition = seekBarRoot.duration * seekHandle.x / bkGrnd.width
//                seeking =false
//            }
//        }
//    }

    Timer {
        id:seekBarTime
        interval: 10
        repeat: true
        running: seeking
        onTriggered: {
            seekBarRoot.seekPosition = seekBarRoot.duration * seekHandle.x / bkGrnd.width
        }
    }

    function formatTime(timeInMs) {
        if (!timeInMs || timeInMs <= 0) return "0:00"
        var seconds = timeInMs / 1000;
        var minutes = Math.floor(seconds / 60)
        seconds = Math.floor(seconds % 60)
        if (seconds < 10) seconds = "0" + seconds;
        return minutes + ":" + seconds
    }

    Text {
        width: 90
        anchors { left: parent.left; top: parent.top; bottom: parent.bottom; leftMargin: 10 }
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        color: "white"
        visible: controlMode ? false : true
        smooth: true
        text: formatTime(playPosition)
    }

    Text {
        width: 90
        anchors { right: parent.right; top: parent.top; bottom: parent.bottom; rightMargin: 10 }
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        color: "white"
        visible: controlMode ? false : true
        smooth: true
        text: formatTime(duration)
    }

}
