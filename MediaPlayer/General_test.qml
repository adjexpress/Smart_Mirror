import QtQuick 2.0
import QtQuick.Shapes 1.12

Shape {
    id:view
    width: 200
    height: 150
    anchors.centerIn: parent
    ShapePath {
        strokeWidth: 4
        strokeColor: "red"
        fillGradient: LinearGradient {
            x1: 20; y1: 20
            x2: 180; y2: 130
            GradientStop { position: 0; color: "blue" }
            GradientStop { position: 0.2; color: "green" }
            GradientStop { position: 0.4; color: "red" }
            GradientStop { position: 0.6; color: "yellow" }
            GradientStop { position: 1; color: "cyan" }
        }
        strokeStyle: ShapePath.SolidLine
        dashPattern: [ 1, 4 ]
        startX:view.width/2; startY:view.height/2
        PathSvg { path: "M 11 10 L 17 10 L 17 26 L 11 26 M 20 10 L 26 10 L 26 26 L 20 26" }
        //PathQuad { x: view.width/2; y: view.height/2; controlX: view.width/2; controlY: view.height/2 }


        //        PathQuad { x: view.width/2; y: 10+view.height/2; controlX: -view.width/2; controlY: 5+view.height/2 }
        //        PathQuad { x: view.width/2; y: view.height/2;controlX:  3*view.width/2; controlY: 5+view.height/2 }

        //        startX: 0; startY: 0;
        //        PathQuad { x: view.width/2; y: -view.height*view.model.length; controlX: -view.width; controlY: view.height/2 }
        //        PathQuad { x: view.width; y: 0; controlX:0; controlY: 0 }
        //        startX: 20; startY: 20
        //        PathLine { x: 180; y: 130 }
        //        PathLine { x: 20; y: 130 }
        //        PathLine { x: 20; y: 20 }

    }
}


//Path {
//            startX: view.width/2; startY: view.height/2
//            PathQuad { x: view.width/2; y: -view.height*view.model.length; controlX: -view.width; controlY: view.height/2 }
//            PathQuad { x: view.width/2; y: view.height/2; controlX: view.width * view.model.length; controlY: view.height/2 }
//        }
