import QtQuick 2.0
import Qt.labs.folderlistmodel 2.10
import QtMultimedia 5.12

Item {
    id: root
    property string str
    property var filter
    property bool fnshd: false


    FolderListModel {
        id:fs
        folder: "file:"+str
        //rootFolder: folder
        nameFilters: filter
        showDirsFirst: true
    }

    Playlist {
        id: plst
    }

    function plstcrtor(){
        fnshd =false
        var path=str

        //fs.folder="file://"+path
        console.log(fs.folder+" "+filter+" "+fs.nameFilters+" "+fs.count+" "+fs.get(0,"fileURL"))
        for(var c=1; fs.count;c++){
            console.log(c+"  "+ fs.get(c,"fileName"))
            if (fs.isFolder(c-1))
                plstcrtor(fs.get(c,"filePath"))
            else{
                plst.addItem(fs.get(c,"fileURL"))
            }
        }
        console.log("finished Adding: "+path)
        fnshd = true
    }


    function log(){
        if(fnshd){
            for(var a=0;plst.itemCount;a++)
                console.log(plst.itemSource(a))
        }
    }
}
