import ".."
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtMultimedia 5.12

Item {
    id:root
    ///////////////////////
    //// Date Types Filters
    ///////////////////////
    property var videoFilters: ["*.mp4","*.mkv","*.3gp","*.mov"];
    property var audioFilters: ["*.ogg","*.amr","*.aac","*.mp3"];

    //////////////////
    //other properties
    //////////////////
    property color  bgColor: "transparent"
    property string source
    property string fileType
    property string currentPath: "file:/home/"   /////must fetch address from QStorageInfo next time
    property bool playState: false
    property bool fReady: false
    property bool mReady: false
    property Content cnt: cntLdr


    Popup{
        id:slctDialog
        //dim: true
        //modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        width: 3*parent.width/4
        height: 3*parent.height/4
        anchors.centerIn: parent
        topInset: 15
        leftInset: 15
        rightInset: 15
        bottomInset: 15
        onOpened: filemanager.loadFm()
        onClosed: slctDialog.clip
        property alias filemanager: filemanager
        Fsb {
            id:filemanager
            anchors.fill: parent
            onFileSelected:{
                fileReady(fileName)
                slctDialog.close()
            }
            onCanceled: slctDialog.close()
            Component.onDestruction: console.log("loadFm finished")
        }
    }



    Content{
        id:cntLdr
        z:2
        anchors.fill: parent
        visible: false
        onInited: playerReady()
    }

    function loadVideo(){
        cntLdr.contentType = "video"
        slctDialog.filemanager.source = currentPath
        slctDialog.filemanager.filters = videoFilters
        slctDialog.open()
    }

    function loadAudio(){
        cntLdr.contentType = "audio"
        slctDialog.filemanager.source = currentPath
        slctDialog.filemanager.filters = audioFilters
        slctDialog.open()
    }

    function fileReady(path){
        fReady = true
        source = path
        console.log("1. "+"fReady is "+fReady+" path is "+path+"\n   initing content")
        cntLdr.source = source
        cntLdr.init()
    }

    function playerReady(){
        mReady = true
        console.log("4. "+"mReady is "+mReady)
        cntLdr.visible = true
        cntLdr.start()
        console.log("5. "+" Started ")
    }
}
