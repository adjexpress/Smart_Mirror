import ".."
import QtQuick 2.0
import Qt.labs.settings 1.1

Item {
    id:settingItem

//    property alias appS:appSetting
//    property alias hwS: hwSetting

    Settings{
        id:appSetting
        category: "Application"
        property alias uiStyle: "qrc:/PathUI.qml"
        property var hsbURL: "http://192.168.1.100:8080"
        property var voiceCmdURL: "https://www.speakpipe.com/voice-recorder"
    }
    Settings{
        id:hwSetting
        category: "HardWare"
        property var resolution
    }
}
