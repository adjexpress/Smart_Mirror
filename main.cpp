#include <QGuiApplication>
#include <QQmlApplicationEngine>
//#include <QFileSystemModel>
//#include <QQmlContext>
#include <QtWebEngine/QtWebEngine>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    app.setOrganizationName("A.Rahamn.A");
    app.setOrganizationDomain("adjexpress");
    app.setApplicationName("Adj_SmartMirror_App");
    QtWebEngine::initialize();

    //QFileSystemModel *model1 = new QFileSystemModel;
    //model1->setRootPath("/home/");

    QQmlApplicationEngine engine;
    //engine.rootContext()->setContextProperty("adj",model1);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
