import ".."
import QtQuick 2.12
import QtQuick.Layouts 1.2
import QtQuick.Window 2.11
import QtQuick.Controls 2.2



Rectangle {
    id:root
    width: applicationWindowWidth
    height: applicationWindowHeight
    onParentChanged: {
        width:parent.width
        height:parent.height
    }
    property int dpi: Math.round(Screen.pixelDensity)
    property int itemHeight: Math.min(width,height)/3
    property int activeAction: axis.height/2 - axis.y

    function  circleX(r,y){
        var x
        x = Math.round(Math.sqrt(Math.pow(r,2)-Math.pow(y,2)))
        return x
    }

    ////////////
    //color sets
    ///////////
    SystemPalette{ id:actPalette; colorGroup: SystemPalette.Active }
    color: "black"
    property color bgColor: actPalette.dark
    property color brdrColor: actPalette.light
    property color hLightColor: actPalette.highlight

    Rectangle {
        id:axis
        width: parent.height*6/5
        height: width
        radius: width/2
        color: parent.color
        x: parent.x - width*6/10
        y: parent.y - (height-parent.height)/2
        border.width: 2
        border.color: "cyan"

        }

    ButtonSmpl {
        id:mscbtn
        y: activeAction - height
        x: circleX(axis.width/2,y) - width/2
        buttonHeight: itemHeight
        buttonWidth: buttonHeight
        buttonColor: bgColor
        borderColor: brdrColor
        onHoverColer: hLightColor
        buttonLabel: ""
        radius: width/2
        onButtonClicked: {
            loadAudio()
        }
        Image {
            id: musicIcon
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/music.svg"
        }
    }


//    GridLayout {
//        id:ly
//        antialiasing: true
//        columns: 3
//        columnSpacing: itemHeight/5
//        rowSpacing: columnSpacing
//        flow: parent.width > parent.height ? GridLayout.LeftToRight : GridLayout.TopToBottom
//        anchors{margins:itemHeight/9 ; horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter}
//        anchors.centerIn: parent

//        ButtonSmpl {
//            id:vidbtn
//            buttonHeight:itemHeight
//            buttonWidth: buttonHeight
//            buttonColor: bgColor
//            borderColor: brdrColor
//            onHoverColer: hLightColor
//            buttonLabel: ""
//            radius: width/2
//            onButtonClicked: {
//                loadVideo()
//            }
//            Image {
//                id: videoIcon
//                anchors.fill: parent
//                fillMode: Image.PreserveAspectFit
//                source: "qrc:/images/video.svg"
//            }
//        }

//        ButtonSmpl {
//            id:mscbtn
//            buttonHeight: itemHeight
//            buttonWidth: buttonHeight
//            buttonColor: bgColor
//            borderColor: brdrColor
//            onHoverColer: hLightColor
//            buttonLabel: ""
//            radius: width/2
//            onButtonClicked: {
//                loadAudio()
//            }
//            Image {
//                id: musicIcon
//                anchors.fill: parent
//                fillMode: Image.PreserveAspectFit
//                source: "qrc:/images/music.svg"
//            }
//        }
//    }

}
