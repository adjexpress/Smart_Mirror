import "./Settings"
import QtQuick 2.9
import QtWebEngine 1.8
import QtQuick.Controls 2.2
import QtQuick.Window 2.12

Item {
    id: webpage
    property var size
    size : Qt.size(applicationWindowWidth,applicationWindowHeight)
    WebEngineView{
        id: homePanel
        anchors.fill: parent
        backgroundColor: "black"

        Component.onCompleted:{
            url= Setting.appS.hsbURL//"https://www.aparat.com"/*"http://192.168.1.100:8080"*/
        }
        onFeaturePermissionRequested: grantFeaturePermission(securityOrigin,WebEngineView.MediaAudioCapture,true)
    }
}
