import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12

Rectangle {
    id: main
    property string hours
    property string minutes
    property string seconds
    property string dates

    anchors.fill: parent
    //width: applicationWindowWidth
    //height: applicationWindowHeight
    onParentChanged: {
        width:parent.width
        height:parent.height
    }
    color: "transparent"

    function formatTime() {
        var date = new Date;
        hours =  date.getHours();
        minutes = date.getMinutes();
        seconds = date.getSeconds();
        dates = date.toLocaleDateString("yyyy:mm:dd");
    }

    Timer{
        interval: 100
        running: true
        onTriggered: formatTime()
        repeat: true
    }

    Rectangle{
        id:time
        color: "black"
        opacity: 0.5
        radius: Math.round(Screen.pixelDensity)
        height: timeText.height
        width: timeText.width
        anchors.top: parent.top
        anchors.left: parent.left
        Text {
            id:timeText
            //anchors.fill: parent
            text: qsTr(hours+" : "+minutes+" : "+seconds)
            font.pixelSize: 25
            //font.family: "B Badr"
            color: "white"
        }
    }

    Rectangle{
        id:date
        color: "black"
        opacity: 0.5
        radius: Math.round(Screen.pixelDensity)
        height: dateText.height
        width: dateText.width
        anchors.top: time.bottom
        anchors.horizontalCenter: time.horizontalCenter
        Text {
            id:dateText
            //anchors.fill: parent
            text:  qsTr(dates)
            font.pixelSize: 18
            //font.family: "B Badr"
            color: "white"
        }
    }
}
