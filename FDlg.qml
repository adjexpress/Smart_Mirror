import "./MediaPlayer"
import QtQuick 2.9
import QtQml.Models 2.3
import QtQuick.Controls 2.2
import Qt.labs.folderlistmodel 2.12
import QtMultimedia 5.12

Page {
    id:fDl
    width: 200; height: 250

    property var  currentModel: videoModel
    property string currentfolder: videoModel.folder
    property var filters : ["*.mp4","*.mkv"];
    property color folderColor: "gold"
    property color fileColor: "lightBlue"
    property bool fnshd: false
    property string str

    function up(){
        var path = currentModel.parentFolder
        if (path.toString().length === 0 || path.toString() === 'file:///')
            return
        videoModel.folder = videoModel.parentFolder
    }
    function down(path){
    }

    //    TestFile{
    //        id:tst
    //    }

    DelegateModel{
        id:dlgModel
        model: currentModel
        delegate: myDelegate
    }

    FolderListModel {
        folder: "file:/home/"
        id: videoModel
        nameFilters: filters
        showDirsFirst: true
    }

    Playlist {
        id: plst
    }

    function plstcrtor(path,index){
        fnshd =false
        videoModel.folder="file:"+path  //videoModel.get(index,"fileURL")

        console.log("index: "+index+"\npath: "+path
                    +"\n new folder: "+videoModel.folder+"\n count: "+videoModel.count+" ")
        switch (videoModel.status) {
        case FolderListModel.Ready :
            for(var c=0; c<videoModel.count;c++){
                console.log(c+"  "+ videoModel.get(c,"fileName"))
                if (videoModel.isFolder(c)){
                    videoModel.folder = videoModel.get(c,"filePath")
                    plstcrtor(videoModel.get(c,"filePath"))}
                else{
                    plst.addItem(videoModel.get(c,"filePath"))
                }
            }
            break;
        case FolderListModel.Loading:
            console.log("loading folders")
        }

        console.log("finished Adding: "+path)
        videoModel.folder = videoModel.parentFolder
        fnshd = true
    }

    function asdf(){
        if(fnshd){
            for(var a=0;a<plst.itemCount;a++)
                console.log(plst.itemSource(a))
        }
    }
    Component {
        id: myDelegate
        Rectangle{
            id:ind
            width: ind.ListView.view.width
            height: txt.contentHeight*3/2
            gradient: grd1

            ButtonSmpl {
                id:add2plst
                z: 2
                buttonLabel: "+"
                width: height
                height: parent.height*2/3
                anchors.right: parent.right
                anchors.margins: 5
                onButtonClicked: {
                    if (videoModel.isFolder(index))
                    {
                        str = videoModel.get(index,"filePath")
                        console.log(str)

                        //tst.filter = filters
                        plstcrtor(str,index);
                    }
                }
            }

            Item {
                id:icon
                width: height
                height: ind.height*2/3
                anchors.left: parent.left
                Image {
                    source: "qrc:///images/folder.svg"
                    fillMode: Image.PreserveAspectFit
                    anchors.margins: 5
                    visible: currentModel.isFolder(index)
                }
            }

            Gradient {
                id:grd1
                GradientStop {position: 0.0; color: "#00000000"}
                GradientStop {position: 1.0; color: "#FF000000"}
            }

            Text {
                id:txt
                text: fileName
                anchors {
                    fill: parent
                    leftMargin: icon.width*1.5
                    //horizontalCenter: parent.horizontalCenter
                    centerIn: parent
                }
                color: videoModel.isFolder(list.currentIndex) ? folderColor : fileColor
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    ind.gradient =  Qt.lighter(grd1 , 1.1)
                    txt.color = Qt.darker(txt.color , 2)
                }
                onExited: {
                    ind.gradient = grd1
                    txt.color= videoModel.isFolder(list.currentIndex) ? folderColor : fileColor
                }
                onClicked: {
                    if (videoModel.isFolder(list.currentIndex))
                        videoModel.folder = fileURL
                    console.log(videoModel.folder + "\n" + fileName+"\n"+list.currentIndex)
                }
            }
        }
    }



    FolderListModel {
        folder: "file:/home/"
        id: musicModel
        nameFilters: ["*.mp3"]
        showDirsFirst: true
    }

    ListView {
        id:list
        anchors.fill: parent
        //interactive: true
        model: dlgModel
        highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
        focus: true
        keyNavigationEnabled: true
    }

    Rectangle {
        id:backrect
        width: 50
        height: 50
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: anchors.rightMargin = 10
        MouseArea{
            anchors.fill: parent
            onClicked: {

                up()//
                console.log(videoModel.folder +"\n"+list.currentIndex)
            }

        }

        Text {
            id: back
            text: qsTr("<")
            anchors.centerIn: parent
        }
    }
}
