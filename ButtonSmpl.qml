import QtQuick 2.9

Rectangle {
    id: sbutt
    width:buttonWidth
    height: buttonHeight
    radius: 3

    property int buttonWidth: parent.width/3
    property int buttonHeight: parent.height*0.9

    property string buttonLabel: "button"

    property color buttonColor: "lightblue"
    property color onHoverColer: "gold"
    property color borderColor: "white"

    border.width: 1
    border.color: borderColor
    antialiasing: true

    Text {
        id: btLabel
        anchors.centerIn: parent
        text: qsTr(buttonLabel)
    }

    signal buttonClicked()
    onButtonClicked:{

    }

    MouseArea {
        id: mousePad
        anchors.fill: parent
        onClicked: buttonClicked()
        hoverEnabled: true
        onEntered: parent.border.color = onHoverColer
        onExited: parent.border.color = borderColor
    }

    color: mousePad.pressed ? Qt.darker(buttonColor , 1.5) : buttonColor
    Behavior on color { ColorAnimation { duration: 55  }     }

    scale: mousePad.pressed ? 1.1 : 1
    Behavior on scale { NumberAnimation { duration: 55 }    }

}
