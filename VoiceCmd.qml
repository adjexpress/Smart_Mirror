import "./Settings"
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick 2.12
import QtWebEngine 1.8

Popup {
    id: popup
    dim: true
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    topInset: 15
    leftInset: 15
    rightInset: 15
    bottomInset: 15
    onClosed: popup.clip
    WebEngineView{
        id: homePanel
        anchors.fill: parent
        backgroundColor: "black"
        Component.onCompleted: url= Setting.appS.voiceCmdURL
        onFeaturePermissionRequested: grantFeaturePermission(securityOrigin,WebEngineView.MediaAudioCapture,true)
    }
}
