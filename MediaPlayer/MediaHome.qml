import ".."
import QtQuick 2.12
import QtQuick.Layouts 1.2
import QtQuick.Window 2.11
import QtQuick.Controls 2.2

Rectangle {
    id:root
    width: applicationWindowWidth
    height: applicationWindowHeight
    onParentChanged: {
        width:parent.width
        height:parent.height
    }
    property int dpi: Math.round(Screen.pixelDensity)
    property int itemHeight: Math.min(width,height)/3

//    MPlayer{
//        id:asd
//        z:4
//        anchors.fill: parent
//    }

    ////////////
    //color sets
    ///////////
    SystemPalette{ id:actPalette; colorGroup: SystemPalette.Active }
    color: "transparent"
    property color bgColor: actPalette.dark
    property color brdrColor: actPalette.light
    property color hLightColor: actPalette.highlight

    GridLayout {
        id:ly
        antialiasing: true
        columns: 3
        columnSpacing: itemHeight/5
        rowSpacing: columnSpacing
        flow: parent.width > parent.height ? GridLayout.LeftToRight : GridLayout.TopToBottom
        anchors{margins:itemHeight/9 ; horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter}
        anchors.centerIn: parent

        ButtonSmpl {
            id:vidbtn
            buttonHeight:itemHeight
            buttonWidth: buttonHeight
            buttonColor: bgColor
            borderColor: brdrColor
            onHoverColer: hLightColor
            buttonLabel: ""
            radius: width/2
            onButtonClicked: {
                mp.loadVideo()
            }
            Image {
                id: videoIcon
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/play2.svg"//"qrc:/images/video.svg"
            }
        }

        ButtonSmpl {
            id:mscbtn
            buttonHeight: itemHeight
            buttonWidth: buttonHeight
            buttonColor: bgColor
            borderColor: brdrColor
            onHoverColer: hLightColor
            buttonLabel: ""
            radius: width/2
            onButtonClicked: {
                mp.loadAudio()
            }
            Image {
                id: musicIcon
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/music.svg"
            }
        }
    }
}
