import".."
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12

Rectangle {
    id:ctrlRoot
    antialiasing: true
//    radius: Screen.PixelDensity*10

    property bool playStatus: true
    property int duration
    property int playPosition
    property int visableTime : 3000
    property string title


    signal close()
    signal play()
    signal pause()
    signal next()
    signal previous()

    gradient: Gradient {
        id:mainRectGrd
        GradientStop {position: 0.0; color: "gray"}
        GradientStop {position: 0.1; color: "transparent"}
        GradientStop {position: 0.9; color: "transparent"}
        GradientStop {position: 1.0; color: "darkgray"}
    }

    Timer {
        id:hideControls
        interval: visableTime
        repeat: false
        onTriggered: ctrlRoot.visible = false
    }
    onVisibleChanged: hideControls.start()

    Rectangle {
        id:titleBar
        width: parent.width
        height: parent.height/11
        anchors.top: parent.top
        color: "transparent"
        opacity: 0.9
        z:2
        Text {
            id: info
            text: title
            anchors.centerIn: parent
        }

        Rectangle {
            id:backBtn
            height: parent.height
            width: height*3/4
            color: "transparent"
            Image {
                id: bkIcon
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/back.svg"
            }
            MouseArea {
                id:bkClick
                height: parent.height*5
                width: height
                anchors.centerIn: parent
                //anchors.fill: parent
                onClicked: loadAudio()
            }
        }

        Rectangle {
            id:closeBtn
            height: parent.height
            width: height
            anchors.right: parent.right
            color: "transparent"
            Image {
                id: clsIcon
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/close.svg"
            }
            MouseArea {
                id:xClick
                height: parent.height*5
                width: height
                anchors.centerIn: parent
                //anchors.fill: parent
                onClicked: close()
            }
        }
    }

    function stateChange() {
        if (playStatus) {
            plyCtl.state = "paused"
            pause()
        }
        else
        {
            plyCtl.state = "baseState"
            play()
        }
    }

    Row {
        id:ctlIndicators
        width: ctrlRoot.width

        anchors{
            verticalCenter: ctrlRoot.verticalCenter
            left: ctrlRoot.left
            right: ctrlRoot.right
            margins: ctrlRoot.width/4
        }
        spacing : ctrlRoot.width/6

        Rectangle {
            id:bwCtl
            Layout.fillWidth: true
            antialiasing: true
            width: ctrlRoot.height/11
            height: width
            color: "transparent"
            Image {
                id: img2
                anchors.fill: parent
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/m_pre.svg"
                mipmap: true
            }
            MouseArea{
                id: prePad
                anchors.fill: parent
                onClicked:previous()
            }

            scale: prePad.pressed ? 1.1 : 1
            Behavior on scale { NumberAnimation { duration: 55 }    }
        }

        Rectangle {
            id:plyCtl
            Layout.fillWidth: true
            antialiasing: true
            width: ctrlRoot.height/11
            height: width
            color: "transparent"
            Image {
                id: img1
                anchors.fill: parent
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/m_pause.svg"
                mipmap: true
            }
            MouseArea{
                id: mousePad
                anchors.fill: parent
                onClicked:stateChange()
            }
            scale: mousePad.pressed ? 1.2 : 1
            Behavior on scale { NumberAnimation { duration: 55 }    }

            states: [
                State {
                    name: "paused"
                    PropertyChanges { target: img1; source:"qrc:/images/m_play.svg"}
                }
            ]
        }

        Rectangle {
            id:fwCtl
            Layout.fillWidth: true
            antialiasing: true
            width: ctrlRoot.height/11
            height: width
            color: "transparent"
            Image {
                id: img3
                anchors.fill: parent
                antialiasing: true
                fillMode: Image.Stretch
                source: "qrc:/images/m_next.svg"
                mipmap: true
            }
            MouseArea{
                id: nxtPad
                anchors.fill: parent
                onClicked:next()
            }

            scale: nxtPad.pressed ? 1.1 : 1
            Behavior on scale { NumberAnimation { duration: 55 }    }
        }

    }



    Rectangle {
        id:ctlBot
        width: parent.width
        height: parent.height/20
        anchors {
            bottom: parent.bottom
            bottomMargin: height/2
        }

        color: "transparent"

        SeekBar {
            id: seekBarRoot
            width: parent.width*0.8
            //height: parent.height/3
            anchors.centerIn: parent
            color: "transparent"
            duration: ctrlRoot.duration
            playPosition: ctrlRoot.playPosition
            onSeekPositionChanged: videoOutput.seek(seekPosition)
        }

    }

}
