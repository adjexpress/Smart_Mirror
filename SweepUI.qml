import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtWebView 1.0
import "./MediaPlayer"

Item {
    z:0
        PathView {
            id: view
            anchors.fill: parent
            flickDeceleration:5
            snapMode: PathView.SnapToItem
            currentIndex: indicator.currentIndex
            model: ["MediaPlayer/MediaHome.qml","WebPage.qml"]
            delegate: Loader {
                width: parent.width
                height: parent.height
                source: modelData
                visible: PathView.isCurrentItem ? 1 : 0
            }
            path: Path {
                startX:view.width/2; startY: view.height/2
                PathLine { x:-view.width/2; y:view.height/2 }
                PathQuad { x:3*view.width/2; y: view.height/2; controlX: view.width/2; controlY: view.height/10 }
                PathLine { x:view.width/2; y:view.height/2 }
            }
        }
        PageIndicator {
            id: indicator
            count: view.count
            interactive: true
            currentIndex: view.currentIndex
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
        }
}
