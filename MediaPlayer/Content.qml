import QtQuick 2.0
import ".."

Rectangle {
    id:cntRoot
    color: "black"
    property string contentType
    property string source
    signal inited()

    Loader{
        id:contentLoader
        onLoaded:
        {
            contentLoader.item.parent = cntRoot
            contentLoader.item.anchors.fill = cntRoot
            contentLoader.item.mediaSource = cntRoot.source
            console.log("2.5  media Source Changed")
            contentLoader.item.finished.connect(cntRoot.stop)
            inited()
            console.log("3. "+"content loader is inited")
        }
    }

    function init(){
        if(contentType!="" && contentType=="video")
        {
            console.log("2. "+"content source is video")
            contentLoader.source =  "VideoPlayer.qml"
        }

        else if(contentType!="" && contentType=="audio")
        {
            console.log("2. "+"content source is audio")
            contentLoader.source = "AudioPlayer.qml"
        }

        else {
            contentLoader.source = ""
            console.log("unable to load content")
        }

    }

    function start(){
        if(contentLoader.item){
            contentLoader.item.start()
        }
    }

    function stop(){
        if(contentLoader.item){
            contentLoader.item.stop()
            //contentLoader.item.mediaSource = ""
            contentLoader.source = ""
            cntRoot.visible = false
        }
    }
}
