import QtQuick 2.0
import Qt.labs.folderlistmodel 2.2

ListView {
    width: 200; height: 400

    FolderListModel {
        folder: "/home/abdorahmanamani"
        id: folderModel
        //nameFilters: ["*.qml"]

    }

    Component {
        id: fileDelegate
        Text { text: fileName }
    }

    model: folderModel
    delegate: fileDelegate
}
